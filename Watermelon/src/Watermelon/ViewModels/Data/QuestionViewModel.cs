﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Watermelon.Models;

namespace Watermelon.ViewModels.Data
{
    public class QuestionViewModel
    {
        [Display(Name = "Id")]
        public int QuestionId { get; set; }
        [Required(ErrorMessage = "Įveskite klausimą.")]
        [Display(Name = "Klausimas")]
        public string QuestionLine { get; set; }
        [Required(ErrorMessage = "Įveskite nuorodą.")]
        [Display(Name = "Nuoroda")]
        public string Url { get; set; }
        [Required(ErrorMessage = "Įveskite neteisingą atsakymą.")]
        [Display(Name = "Pirmas neteisingas atsakymas")]
        public string WrongAns1 { get; set; }
        [Required(ErrorMessage = "Įveskite neteisingą atsakymą.")]
        [Display(Name = "Antras neteisingas atsakymas")]
        public string WrongAns2 { get; set; }
        [Required(ErrorMessage = "Įveskite neteisingą atsakymą.")]
        [Display(Name = "Trečias neteisingas atsakymas")]
        public string WrongAns3 { get; set; }
        [Required(ErrorMessage = "Įveskite teisingą atsakymą.")]
        [Display(Name = "Teisingas atsakymas")]
        public string CorrectAns { get; set; }  // Teisingas atsakymas
        [Required(ErrorMessage = "Pasirinkite kategoriją.")]
        [Display(Name = "Kategorija")]
        public string Category { get; set; }

        public IEnumerable <Category> categories { get; set; }

        public Question GetQuestion()
        {
            Question Question = new Question();
            Question.QuestionLine = QuestionLine;
            Question.Url = Url;
            Question.WrongAns1 = WrongAns1;
            Question.WrongAns2 = WrongAns2;
            Question.WrongAns3 = WrongAns3;
            Question.CorrectAns = CorrectAns;
            Question.Category = Category;
            return Question;
        }

        public Question GetQuestionWithId()
        {
            Question Question = new Question();
            Question.QuestionId = QuestionId;
            Question.QuestionLine = QuestionLine;
            Question.Url = Url;
            Question.WrongAns1 = WrongAns1;
            Question.WrongAns2 = WrongAns2;
            Question.WrongAns3 = WrongAns3;
            Question.CorrectAns = CorrectAns;
            Question.Category = Category;
            return Question;
        }

        public void SetQuestion(Question Question)
        {
            QuestionId = Question.QuestionId;
            QuestionLine = Question.QuestionLine;
            Url = Question.Url;
            WrongAns1 = Question.WrongAns1;
            WrongAns2 = Question.WrongAns2;
            WrongAns3 = Question.WrongAns3;
            CorrectAns = Question.CorrectAns;
            Category = Question.Category;
        }
    }
}