﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Watermelon.ViewModels.Account
{
    public class AccountInfoDisplayModel
    {
        public String Username { get; set; }
        public String Email { get; set; }
    }
}
