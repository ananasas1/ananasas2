﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Watermelon.ViewModels.Account
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Įveskite el. pašta.")]
        [EmailAddress]
        [Display(Name = "El. paštas")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Įveskite slaptažodį.")]
        [DataType(DataType.Password)]
        [Display(Name = "Slaptažodis")]
        public string Password { get; set; }

        [Display(Name = "Įsiminti?")]
        public bool RememberMe { get; set; }
    }
}
