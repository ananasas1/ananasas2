﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Watermelon.ViewModels.Account
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "Įveskite slapyvardį.")]
        [Display(Name = "Slapyvardis")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Įveskite el. pašto adresą.")]
        [EmailAddress(ErrorMessage = "Netinkasmas el. paštas.")]
        [Display(Name = "El. paštas")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Įveskite slaptažodį.")]
        [StringLength(100, ErrorMessage = "Slaptažodis turi būti bent {2} simbolių ilgio.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Slaptažodis")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Pakartokite slaptažodį")]
        [Compare("Password", ErrorMessage = "Slaptažodžiai nesutampa.")]
        public string ConfirmPassword { get; set; }
    }
}
