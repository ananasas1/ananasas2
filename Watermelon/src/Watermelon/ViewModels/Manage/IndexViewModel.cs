﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace Watermelon.ViewModels.Manage
{
    public class IndexViewModel
    {
        public String Name { get; set; }
        public String Email { get; set; }

    }
}
