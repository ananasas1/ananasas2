﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Watermelon.ViewModels.Manage
{
    public class ChangePasswordViewModel
    {
        [Required(ErrorMessage = "Įveskite dabartinį slaptažodį.")]
        [DataType(DataType.Password)]
        [Display(Name = "Dabartinis slaptažodis")]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "Įveskite naują slaptažodį.")]
        [StringLength(100, ErrorMessage = "Slaptažodis turi būti bent {2} simbolių ilgio.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Naujas slaptažodis")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Pakartokite naują slaptažodį")]
        [Compare("NewPassword", ErrorMessage = "Slaptažodžiai nesutampa.")]
        public string ConfirmPassword { get; set; }
    }
}
