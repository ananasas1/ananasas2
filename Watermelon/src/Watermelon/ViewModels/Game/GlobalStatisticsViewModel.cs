﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Watermelon.ViewModels.Game
{
    public class GlobalStatisticsViewModel
    {
        public List<PlayerStatisticsViewModel> Statistics { get; set; }
        public int PlayerNumber { get; set; }  // Reikalingas, kad žinot kuri eilutė yra žaidėjo
    }
}
