﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Watermelon.Models;

namespace Watermelon.ViewModels.Game
{
    public class PlayerStatisticsViewModel
    {
        public String PlayerName { get; set; }
        public int AnswerIn1 { get; set; }  // Kiek kartų iš pirmo karto atspėjo
        public int PlayedGamesCount { get; set; }
        public int Points { get; set; }  // Žaidėjo taškai
        public double Score { get; set; }

    }
}
