﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Watermelon.ViewModels.Game
{
    public class GameResultsViewModel
    {
        public int ans1 { get; set; }
        public int ans2 { get; set; }
        public int ans3 { get; set; }
        public int ans4 { get; set; }
    }
}
