﻿using Microsoft.AspNet.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Watermelon.Models;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Authorization;
using Watermelon.ViewModels.Data;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Watermelon.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class DataController : Controller
    {
        private GameDbContext _context;

        public DataController(GameDbContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {

            return View(_context.Questions.ToList());
        }

        public IActionResult CreateQuestion()
        {
            QuestionViewModel qC = new QuestionViewModel();
            qC.categories = _context.Categories.ToList();
            return View(qC);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CreateQuestion(QuestionViewModel Model)
        {
           if (ModelState.IsValid)
            {
                _context.Questions.Add(Model.GetQuestion());
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            Model.categories = _context.Categories.ToList();
            return View(Model);
        }

        public IActionResult EditQuestion(int id)
        {
            QuestionViewModel qC = new QuestionViewModel();
            qC.categories = _context.Categories.ToList();
            qC.SetQuestion(_context.Questions
            .Where(c => c.QuestionId == id)
            .Single());
            return View(qC);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EditQuestion(QuestionViewModel Model)
        {
            if (ModelState.IsValid)
            {
                _context.Questions.Update(Model.GetQuestionWithId());
                _context.SaveChanges();

                return RedirectToAction("Index");
            }
            Model.categories = _context.Categories.ToList();
            return View(Model);
        }


        [ActionName("DeleteQuestion")]
        public IActionResult DeleteQuestion1(int id)
        {
            var question = _context.Questions
            .Where(c => c.QuestionId == id)
            .Single();

            _context.Questions.Remove(question);
            _context.SaveChanges();

            return View(question);
        }

        [HttpPost, ActionName("DeleteQuestion")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteQuestion(int id)
        {
            var question = _context.Questions
            .Where(c => c.QuestionId == id)
            .Single();

            _context.Questions.Remove(question);
            _context.SaveChanges();

            return RedirectToAction("Index");
        }

        public IActionResult CategoriesList()
        {
            return View(_context.Categories.ToList());
        }

        public IActionResult CreateCategory()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CreateCategory(Category category)
        {
            if (ModelState.IsValid)
            {
                _context.Categories.Add(category);
                _context.SaveChanges();
                return RedirectToAction("CategoriesList");
            }
            return View();
        }

        public IActionResult EditCategory(int id)
        {
            var category = _context.Categories
                .Where(c => c.CategoryId == id)
                .Single();
            return View(category);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EditCategory(Category category)
        {
            if (ModelState.IsValid)
            {
                _context.Categories.Update(category);
                _context.SaveChanges();
                return RedirectToAction("CategoriesList");
            }

            return View(category);
        }

        [ActionName("DeleteCategory")]
        public IActionResult DeleteCategory1(int id)
        {
            var category = _context.Categories
                .Where(c => c.CategoryId == id)
                .Single();

            _context.Categories.Remove(category);
            _context.SaveChanges();

            return View(category);
        }

        [HttpPost, ActionName("DeleteCategory")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteCategory(int id)
        {
            var category = _context.Categories
                .Where(c => c.CategoryId == id)
                .Single();

            _context.Categories.Remove(category);
            _context.SaveChanges();

            return RedirectToAction("Index");
        }
       
    }
}
