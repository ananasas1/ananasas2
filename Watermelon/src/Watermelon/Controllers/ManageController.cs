﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Mvc;
using Microsoft.Extensions.Logging;
using Watermelon.Models;
using Watermelon.ViewModels.Manage;
using Watermelon.ViewModels.Game;
using Microsoft.Data.Entity;

namespace Watermelon.Controllers
{
    [Authorize(Roles = "Player, Administrator")]
    public class ManageController : Controller
    {
        private /*readonly*/ UserManager<PlayerAccount> _userManager;
        private readonly SignInManager<PlayerAccount> _signInManager;
        private readonly ILogger _logger;

        public ManageController(
        UserManager<PlayerAccount> userManager,
        SignInManager<PlayerAccount> signInManager,
        ILoggerFactory loggerFactory)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = loggerFactory.CreateLogger<ManageController>();
        }

        [HttpGet]
        public async Task<IActionResult> Index(ManageMessageId? message = null)
        {
            ViewData["StatusMessage"] =
                message == ManageMessageId.ChangePasswordSuccess ? "Slaptažodis sėkmingai pakeistas."
                : "";

            var user = await GetCurrentUserAsync();
            PlayerAccount model = new PlayerAccount();
            model.Email = user.Email;
            model.Name = user.Name;
            model.RealName = user.RealName;
            model.Birthday = user.Birthday;

            return View(model);
        }

        [HttpGet]
        public IActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                var result = await _userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
                if (result.Succeeded)
                {
                    await _signInManager.SignInAsync(user, isPersistent: false);
                    _logger.LogInformation(3, "User changed their password successfully.");
                    return RedirectToAction(nameof(Index), new { Message = ManageMessageId.ChangePasswordSuccess });
                }
                AddErrors(result);
                return View(model);
            }
            return RedirectToAction(nameof(Index), new { Message = ManageMessageId.Error });
        }

        [HttpPost]
        public async Task<IActionResult> Index(PlayerAccount profile)
        {
            var usr = await GetCurrentUserAsync();
            if (usr != null)
            {
                if(profile.Name != null)
                    usr.Name = profile.Name;
                if (profile.Birthday != null)
                    usr.Birthday = profile.Birthday;
                if (profile.RealName != null)
                    usr.RealName = profile.RealName;
            }

            profile.Email = usr.Email;
            profile.RealName = usr.RealName;
            profile.Birthday = usr.Birthday;
            profile.Name = usr.Name;

            var result = await _userManager.UpdateAsync(usr);

            if (!result.Succeeded)
            {
                AddErrors(result);
            }

            return View(profile);
        }

        #region Helpers
        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            Error
        }

        private async Task<PlayerAccount> GetCurrentUserAsync()
        {
            return await _userManager.FindByIdAsync(HttpContext.User.GetUserId());
        }
        #endregion
    }
}
