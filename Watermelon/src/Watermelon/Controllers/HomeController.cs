﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Watermelon.Models;

namespace Watermelon.Controllers
{ 

    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Apie";

            return View();
        }

        public IActionResult SomethingWrong()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
