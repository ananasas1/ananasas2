﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Watermelon.Models;
using Microsoft.AspNet.Authorization;
using Watermelon.ViewModels.Game;
using Microsoft.Data.Entity;
using Microsoft.AspNet.Identity;
using System.Security.Claims;

namespace Watermelon.Controllers
{
    public class StatisticsController: Controller
    {

        private readonly UserManager<PlayerAccount> _userManager;
        private GameDbContext _context;

        public StatisticsController(GameDbContext context, UserManager<PlayerAccount> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public IActionResult Index()
        {
            List<PlayerStatistics> StatisticsList = _context.PlayerStatistics
                .Include( c => c.Player)
                .OrderByDescending(c => c.GetScore())
                .Take(100)
                .ToList();

            List<PlayerStatisticsViewModel> GlobalStatistics = new List<PlayerStatisticsViewModel>();

            int PlayerNumber = -1;
            for (int i = 0; i < StatisticsList.Count; i++)
            {
                PlayerStatisticsViewModel Model = new PlayerStatisticsViewModel();
                var Statistics = StatisticsList.ElementAt(i);
                Model.AnswerIn1 = Statistics.AnswerIn1;

                var Player = Statistics.Player;

                Model.PlayerName = Statistics.Player.Name;
                if (Statistics.Player.UserName == User.Identity.Name)
                    PlayerNumber = i;

                Model.PlayedGamesCount = Statistics.PlayedGamesCount;
                Model.Points = Statistics.GetPoints();
                Model.Score = Statistics.GetScore();
                GlobalStatistics.Add(Model);
            }
            var stat = new GlobalStatisticsViewModel();
            stat.Statistics = GlobalStatistics;
            stat.PlayerNumber = ++PlayerNumber;

            return View(stat);
        }


        public IActionResult Statistics()
        {
            return View();
        }

        [Authorize(Roles = "Player")]
        public async Task<IActionResult> Chart()
        {
            var user = _context.PlayerStatistics.Where(c => c.Player.UserName == User.Identity.Name).Single();
            if (user != null)
            {
                return View(user);
            }
            return Redirect("~/Home/SomethingWrong");
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult resetResults()
        {
            if (!User.Identity.IsAuthenticated)
                return Json(new { msg = "neprisijunges" });
            var user = _context.PlayerStatistics.Where(c => c.Player.UserName == User.Identity.Name).Single();
            if (user != null)
            {
                user.PlayedGamesCount++;
                user.AnswerIn1 = 0;
                user.AnswerIn2 = 0;
                user.AnswerIn3 = 0;
                user.AnswerIn4 = 0;
                user.PlayedGamesCount = 0;
                _context.SaveChanges();
            }

            return Redirect("/Statistics/Chart");//Json(new { /*msg = "bar"*/ });
        }

        #region Helpers

        private async Task<PlayerAccount> GetCurrentUserAsync()
        {
            return await _userManager.FindByIdAsync(HttpContext.User.GetUserId());
        }

        #endregion

    }
}
