﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Watermelon.Models;
using Watermelon.ViewModels.Game;
using Microsoft.AspNet.Http;
using Newtonsoft.Json;

namespace Watermelon.Controllers
{
    public class GameController : Controller
    {
        private GameDbContext _context;

        public GameController(GameDbContext context)
        {
            _context = context;
        }
            
        public IActionResult Question()
        {
            List<Question> a = _context.Questions.ToList();
            Random ran = new Random(DateTime.Now.Millisecond);
            int quesNr = ran.Next(0, a.Count);
            if (a.Count == 0)
            {
                return RedirectToAction("Index", "Home");
            }  
            return View(a);
        }
        [HttpPost]
        public IActionResult Question(String category)
        {
            HttpContext.Session.Clear();
            List<Question> a = _context.Questions
                .Where(c => c.Category == category)
                .ToList();
            if (a.Count == 0)
            {
                return RedirectToAction("Index", "Home");
            }
            return View(a);
        }

        public IActionResult UpdateStats(string results)
        {
            GameResultsViewModel res = JsonConvert.DeserializeObject<GameResultsViewModel>(results);
            HttpContext.Session.SetInt32("AnswerIn1", res.ans1);
            HttpContext.Session.SetInt32("AnswerIn2", res.ans2);
            HttpContext.Session.SetInt32("AnswerIn3", res.ans3);
            HttpContext.Session.SetInt32("AnswerIn4", res.ans4);
            if (!User.Identity.IsAuthenticated)
                return Json(new { msg = "neprisijunges" });
            var user = _context.PlayerStatistics.Where(c => c.Player.UserName == User.Identity.Name).Single();
            if (user != null)
            {
                user.PlayedGamesCount++;
                user.AnswerIn1 += res.ans1;
                user.AnswerIn2 += res.ans2;
                user.AnswerIn3 += res.ans3;
                user.AnswerIn4 += res.ans4;
                _context.SaveChanges();
            }

            return Json(new { msg = "bar"});
        }

        public IActionResult EndScreen()
        {
            var Model = new GameResultsViewModel();
            Model.ans1 = Convert.ToInt32(HttpContext.Session.GetInt32("AnswerIn1"));
            Model.ans2 = Convert.ToInt32(HttpContext.Session.GetInt32("AnswerIn2"));
            Model.ans3 = Convert.ToInt32(HttpContext.Session.GetInt32("AnswerIn3"));
            Model.ans4 = Convert.ToInt32(HttpContext.Session.GetInt32("AnswerIn4"));

            return View(Model);
        }

        public IActionResult ChooseCategory()
        {
            return View(_context.Categories.ToList());
        }

    }
}
