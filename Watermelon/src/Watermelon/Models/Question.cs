﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Watermelon.Models
{
    public class Question
    {
        [Key]
        public int QuestionId { get; set; }
        [Required(ErrorMessage = "Įveskite klausimą.")]
        [Display(Name = "Klausimas")]
        public string QuestionLine { get; set; }
        [Required(ErrorMessage = "Įveskite nuorodą.")]
        [Display(Name = "Nuoroda")]
        public string Url { get; set; }
        [Required(ErrorMessage = "Įveskite neteisingą atsakymą.")]
        [Display(Name = "Pirmas neteisingas atsakymas")]
        public string WrongAns1 { get; set; }  // Neteisingi atsakymai
        [Required(ErrorMessage = "Įveskite neteisingą atsakymą.")]
        [Display(Name = "Antras neteisingas atsakymas")]
        public string WrongAns2 { get; set; }
        [Required(ErrorMessage = "Įveskite neteisingą atsakymą.")]
        [Display(Name = "Trečias neteisingas atsakymas")]
        public string WrongAns3 { get; set; }
        [Required(ErrorMessage = "Įveskite teisingą atsakymą.")]
        [Display(Name = "Teisingas atsakymas")]
        public string CorrectAns { get; set; }  // Teisingas atsakymas
        [Required(ErrorMessage = "Pasirinkite kategoriją.")]
        [Display(Name = "Kategorija")]
        public string Category { get; set; }

        public Question SetQuestion(string QuestionLine, string WrongAns1, string WrongAns2,
            string WrongAns3, string CorrectAns, string Url, string Category)
        {
            Question tmp = new Question();
            tmp.QuestionLine = QuestionLine;
            tmp.Url = Url;
            tmp.WrongAns1 = WrongAns1;
            tmp.WrongAns2 = WrongAns2;
            tmp.WrongAns3 = WrongAns3;
            tmp.CorrectAns = CorrectAns;
            tmp.Category = Category;
            return tmp;
        }

    }
}
