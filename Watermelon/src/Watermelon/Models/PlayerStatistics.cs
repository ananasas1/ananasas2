﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Watermelon.Models
{
    public class PlayerStatistics
    {
        [Key]
        public int StatId { get; set; }
        public int AnswerIn1 { get; set; }  // Kiek kartų iš pirmo karto atspėjo
        public int AnswerIn2 { get; set; }  // Kiek kartų iš antro karto atspėjo
        public int AnswerIn3 { get; set; }  // Kiek kartų iš trečio karto atspėjo
        public int AnswerIn4 { get; set; }
        public int PlayedGamesCount { get; set; }
        public string PlayerId { get; set; }
        public PlayerAccount Player { get; set; }

        public PlayerStatistics()
        {
            AnswerIn1 = 0;
            AnswerIn2 = 0;
            AnswerIn3 = 0;
            AnswerIn4 = 0;
            PlayedGamesCount = 0;
        }

        public int GetPoints()
        {
            return AnswerIn1 * 5 + AnswerIn2 * 3 + AnswerIn3;
        }

        public double GetScore()
        {
            return Math.Round((double) GetPoints() / (AnswerIn1 + AnswerIn2 + AnswerIn3 + AnswerIn4), 2);
        }

    
    }
}
