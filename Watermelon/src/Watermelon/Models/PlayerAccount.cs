﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Watermelon.Models
{
    public class PlayerAccount : IdentityUser
    {
        public String Name { get; set; }
        public string RealName { get; set; }
        public DateTime Birthday { get; set; }
        public virtual PlayerStatistics PlayerStatistics { get; set; }
    }
}
