﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Watermelon.Models
{
    public class Category
    {
        [Key]
        [Display(Name = "Kategorijos numeris")]
        public int CategoryId { get; set; }

        [Required(ErrorMessage = "Įveskite kategorijos pavadinimą.")]
        [Display(Name = "Pavadinimas")]
        public string CategoryName { get; set; }

        [Required(ErrorMessage = "Įveskite kategorijos eilutę.")]
        [Display(Name = "Kategorijos apibūdinimas")]
        public string CategoryLine { get; set; }

    }
}
