﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.AspNet.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Watermelon.Models
{

    public class GameDbContext : IdentityDbContext<PlayerAccount>
    {
        public DbSet<PlayerStatistics> PlayerStatistics { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Category> Categories { get; set; }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<PlayerAccount>()
                .ForSqlServerToTable("AspNetUsers")
                .HasOne(p => p.PlayerStatistics)
                .WithOne(i => i.Player)
                .HasForeignKey<PlayerStatistics>(b => b.PlayerId);
        }

    }
}